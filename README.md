# Slideshow Toolbar
Slideshow Toolbar using slideshowimageview

## Entry Running Requirements
1. Use DevEco Studio and download the SDK.
2. Change the dependencies→classpath version in the build.gradle file to the corresponding version. (that is, the version used in your IDE's new project)


### How it works?
Solution 1: local har package integration
1. Add the har package to the lib folder.
2. Add the following code to gradle of the entry:
```groovy
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

Solution 2:
```groovy
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:SlideshowToolbar:1.0.2'
```

### Demo
![SlideshowToolbar](https://gitee.com/openharmony-tpc/SlideshowToolbar/raw/master/screenshot/demo.gif)

# Usage

* For setting images
```java
 slideshowImageView.setImages(ResourceTable.Media_test1, ResourceTable.Media_test2, ResourceTable.Media_test3);
```

* For adding images
```java
 slideshowImageView.addImages(ResourceTable.Media_test3, ResourceTable.Media_test4, ResourceTable.Media_test5);
```




package com.funtory.slideshowimageview.viewmodel;

import com.funtory.slideshowimageview.SlideshowImageView;
import com.funtory.slideshowimageview.constants.Anim;
import ohos.app.Context;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;


public final class SlideshowViewModel {

    private List<Integer> images = new ArrayList<>();

    private SecureRandom random;

    private float[] animRangeX = null;
    private float[] animRangeY = null;

    private boolean isAnimate = false;
    private int currentChildIndex = -1;

    public SlideshowViewModel(){
        random = new SecureRandom();
    }

    public void setImages(int... resIds){
        images.clear();
        addImages(resIds);
    }

    public void addImages(int... resIds) {
        for (int resId : resIds) {
            images.add(resId);
        }
    }

    public int getImageCount(){
        return images.size();
    }

    /**
     * 현재 child에 세팅되어있는 index 외에서 랜덤한 값을 얻는다.
     * @param curIndex curIndex
     * @return RandomImageIndex
     */
    public int getRandomImageIndex(List<Integer> curIndex) {
        List<Integer> candi = new ArrayList<>();

        for (int i = 0; i < images.size(); i++) {
            if (!curIndex.contains(i)) {
                candi.add(i);
            }
        }

        if (candi.size() == 0) {
            return -1;
        } else if (candi.size() == 1) {
            return candi.get(0);
        } else {
            return candi.get(random.nextInt(candi.size()));
        }
    }

    public int getImageId(Context context, int imageIndex){
        return images.get(imageIndex);
    }

    private void initRange(int viewW, int viewH) {
        float x = ((viewW * Anim.SCALE) - viewW) / 2.0f;
        float y = ((viewH * Anim.SCALE) - viewH) / 2.0f;
        animRangeX = new float[]{x, -x};
        animRangeY = new float[]{y, -y};
    }

    public void updateAnimConfig(SlideshowImageView src, int targetChildIndex){
        currentChildIndex = targetChildIndex;

        isAnimate = true;
        if (animRangeX == null || animRangeY == null) {
            initRange(src.getWidth(), src.getHeight());
        }
    }

    public void initAnimConfig(){
        currentChildIndex = 0;

        isAnimate = false;

        animRangeX = null;
        animRangeY = null;
    }

    public float getRangeX() {
        return animRangeX[random.nextInt(animRangeX.length)];
    }

    public float getRangeY() {
        return animRangeY[random.nextInt(animRangeY.length)];
    }

    public boolean isAnimate(){
        return isAnimate;
    }

    public int getCurrentChildIndex(){
        return currentChildIndex;
    }
}

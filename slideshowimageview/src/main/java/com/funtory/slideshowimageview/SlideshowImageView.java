package com.funtory.slideshowimageview;

import com.funtory.slideshowimageview.constants.Anim;
import com.funtory.slideshowimageview.viewmodel.SlideshowViewModel;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;

import java.util.ArrayList;
import java.util.List;

public class SlideshowImageView extends DependentLayout implements Component.BindStateChangedListener {

    private SlideshowViewModel slideshowViewModel = new SlideshowViewModel();
    private List<AnimatorGroup> animatorSets = new ArrayList<>();

    private EventHandler loopHandler;

    public SlideshowImageView(Context context) {
        super(context);
        init();
    }

    public SlideshowImageView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public SlideshowImageView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        Image imageView = new Image(getContext());
        imageView.setScaleMode(Image.ScaleMode.CLIP_CENTER);
        addComponent(imageView, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));

        imageView = new Image(getContext());
        imageView.setScaleMode(Image.ScaleMode.CLIP_CENTER);
        addComponent(imageView, new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));

        setBindStateChangedListener(SlideshowImageView.this);
    }

    private Image getImageView(int index) {
        return (Image) getComponentAt(index);
    }

    /**
     * 새로운 이미지들로 교체.
     *
     * @param resIds
     */
    public void setImages(int... resIds) {
        for (int i = 0; i < getChildCount(); i++) {
            getImageView(i).setTag(null);
        }

        slideshowViewModel.setImages(resIds);
        tryAnim(resIds);
    }

    /**
     * 새로운 이미지 추가.
     *
     * @param resIds
     */
    public void addImages(int... resIds) {
        slideshowViewModel.addImages(resIds);
        tryAnim(resIds);
    }

    private void tryAnim(int... resIds){
        if (slideshowViewModel.getImageCount() > 1) {
            if (!slideshowViewModel.isAnimate()) {
                final int currentChildIndex = slideshowViewModel.getCurrentChildIndex();
                if (currentChildIndex > -1) {
                    gone(currentChildIndex);
                    getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            anim(Math.abs(currentChildIndex - 1));
                        }
                    }, Anim.DURATION / 4);
                } else {
                    getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
                                @Override
                                public void run() {
                                    anim(0);
                                }
                            }, 10);

                        }
                    });
                }
            }
        } else {
            getImageView(0).setPixelMap(resIds[0]);
            getImageView(0).setTag(0);

            slideshowViewModel.initAnimConfig();

        }
    }

    private void gone(int targetChildIndex) {
        Image target = getImageView(targetChildIndex);
        AnimatorProperty goneAlpha = target.createAnimatorProperty();
        goneAlpha.alphaFrom(1.0f).alpha(0.0f);
        goneAlpha.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                animator.release();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        goneAlpha.setDuration(Anim.DURATION);

        goneAlpha.start();
    }


    private List<Integer> getCurrentIndex(){
        List<Integer> result = new ArrayList<>();
        for(int i = 0 ; i < getChildCount() ; i++){
            result.add(getComponentAt(i).getTag() == null ? -1 : (Integer) getComponentAt(i).getTag());
        }

        return result;
    }

    private void anim(final int targetChildIndex) {
        if(animatorSets == null){
            return;
        }

        slideshowViewModel.updateAnimConfig(this, targetChildIndex);

        final Image target = getImageView(targetChildIndex);
        target.setScaleX(Anim.SCALE);
        target.setScaleY(Anim.SCALE);

        int imageIndex = slideshowViewModel.getRandomImageIndex(getCurrentIndex());

        if (imageIndex > -1) {
            target.setTag(imageIndex);
            target.setPixelMap(null);
            target.setVisibility(Component.INVISIBLE);
            target.setPixelMap(slideshowViewModel.getImageId(getContext(), imageIndex));
        }

        float rangeX = slideshowViewModel.getRangeX();
        float rangeY = slideshowViewModel.getRangeY();
        AnimatorProperty trans = target.createAnimatorProperty();
        trans.moveFromX(rangeX).moveToX(-rangeX).moveFromY(rangeY).moveToY(-rangeY);
        trans.setDuration(Anim.DURATION);

        AnimatorProperty alpha = target.createAnimatorProperty();
        alpha.alphaFrom(0.0f).alpha(1.0f);
        alpha.setDuration(Anim.DURATION / 2);

        AnimatorProperty alphaAfter = target.createAnimatorProperty();
        alphaAfter.alphaFrom(1.0f).alpha(0.0f);
        alphaAfter.setDuration(Anim.DURATION / 2);
        alphaAfter.setDelay(Anim.DURATION / 2);

        final AnimatorGroup animSet = new AnimatorGroup();
        animSet.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animSet.runParallel(trans, alpha, alphaAfter);
        animSet.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

                getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
                    @Override
                    public void run() {
                        target.setVisibility(Component.VISIBLE);
                    }
                }, 10);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                animator.release();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorSets.add(animSet);
        animSet.start();

        getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                anim(Math.abs(targetChildIndex - 1));
            }
        }, Anim.DURATION / 2);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        for(AnimatorGroup animatorSet : animatorSets){
            animatorSet.end();
        }
        animatorSets.clear();
        animatorSets = null;
    }
}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.funtory.slideshowapp.slice;

import com.funtory.slideshowapp.ResourceTable;
import com.funtory.slideshowimageview.SlideshowImageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

/**
 * 示例
 */
public class MainAbilitySlice extends AbilitySlice {

    private SlideshowImageView iv_expand;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        iv_expand = (SlideshowImageView)findComponentById(ResourceTable.Id_iv_expand);
        iv_expand.setImages(ResourceTable.Media_test1, ResourceTable.Media_test7);
        iv_expand.addImages(ResourceTable.Media_test2, ResourceTable.Media_test3);
        iv_expand.addImages(ResourceTable.Media_test4, ResourceTable.Media_test5, ResourceTable.Media_test6);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
